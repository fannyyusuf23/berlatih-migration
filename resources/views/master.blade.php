
<!DOCTYPE html>
<html lang="en">
  <head>
    @include('layout.head')
  </head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
 @include('layout.header')
  @include('layout.sidebar')
  <!-- /.navbar -->

   @yield('content')

  <!-- Content Wrapper. Contains page content -->
  
  <!-- /.content-wrapper -->
   @include('layout.footer')


<!-- ./wrapper -->
<!-- jQuery -->
 @include('layout.footer-scripts')
 @stack('scripts')
</body>
</html>
