<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

route::get('register', "AuthController@register");
route::get('welcome',"AuthController@welcome");

Route::get('/master', function () {
    return view('master');
});

Route::get('/table', function () {
    return view('table');
});

Route::get('/datatable', function () {
    return view('datatable');
});